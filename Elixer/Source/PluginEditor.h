/*
  ==============================================================================

	This file was auto-generated!

	It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class ElixerAudioProcessorEditor : public AudioProcessorEditor
{
public:
	ElixerAudioProcessorEditor(ElixerAudioProcessor&);
	~ElixerAudioProcessorEditor();
	//==============================================================================
	void paint(Graphics&) override;
	void resized() override;
	int currentOscillatorIndex;

private:
	// This reference is provided as a quick way for your editor to
	// access the processor object that created it.
	ElixerAudioProcessor& processor;
	Slider oscillatorCount;
	Slider frequencyOffset;
	Slider amplification;
	Label oscillatorCountLable;
	Label frequencyOffsetLable;
	Label amplificationLable;
	ToggleButton oscillatorButtons[10];

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ElixerAudioProcessorEditor)
};
