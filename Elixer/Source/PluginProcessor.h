#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class SineOscillator
{
public:
	SineOscillator() {}
	double volume = 0;

	void setFrequency(float frequency, float sampleRate)
	{
		auto cyclesPerSample = frequency / sampleRate;
		angleDelta = cyclesPerSample * MathConstants<float>::twoPi;
	}

	void setVolume(double volume) {
		this->volume = volume;
	}

	double getVolume() {
		return this->volume;
	}

	forcedinline void updateAngle() noexcept
	{
		currentAngle += angleDelta;
		if (currentAngle >= MathConstants<float>::twoPi)
			currentAngle -= MathConstants<float>::twoPi;
	}

	forcedinline float getNextSample() noexcept
	{
		auto currentSample = std::sin(currentAngle);
		updateAngle();
		return currentSample;
	}

private:
	float currentAngle = 0.0f, angleDelta = 0.0f;
};

//==============================================================================
/**
*/
class ElixerAudioProcessor : public AudioProcessor
{
public:
	//==============================================================================
	ElixerAudioProcessor();
	~ElixerAudioProcessor();

	//==============================================================================
	void prepareToPlay(double sampleRate, int samplesPerBlock) override;

	void releaseResources() override;

#ifndef JucePlugin_PreferredChannelConfigurations
	bool isBusesLayoutSupported(const BusesLayout& layouts) const override;
#endif

	void processBlock(AudioBuffer<float>&, MidiBuffer&) override;

	//==============================================================================
	AudioProcessorEditor* createEditor() override;
	bool hasEditor() const override;

	//==============================================================================
	const String getName() const override;

	bool acceptsMidi() const override;
	bool producesMidi() const override;
	bool isMidiEffect() const override;
	double getTailLengthSeconds() const override;

	//==============================================================================
	int getNumPrograms() override;
	int getCurrentProgram() override;
	void setCurrentProgram(int index) override;
	const String getProgramName(int index) override;
	void changeProgramName(int index, const String& newName) override;

	//==============================================================================
	void getStateInformation(MemoryBlock& destData) override;
	void setStateInformation(const void* data, int sizeInBytes) override;
	//==============================================================================
	int numberOfOscillators = 1;
	double frequencyOffsets[10];
	double amplificationOffsets[10];

private:
	//==============================================================================
	float level = 0.0f;
	double midiNote = 0;
	OwnedArray<SineOscillator> oscillators;
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ElixerAudioProcessor)
};
