//Photo by Spencer Imbrock on Unsplash

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <math.h>
using namespace std;

void setupCustomLookAndFeelColours(LookAndFeel& laf)
{
	laf.setColour(Slider::thumbColourId, Colour::greyLevel(0.95f));
	laf.setColour(Slider::textBoxOutlineColourId, Colours::transparentWhite);
	laf.setColour(Slider::rotarySliderFillColourId, Colour(0xff00b5f6));
	laf.setColour(Slider::rotarySliderOutlineColourId, Colours::white);

	laf.setColour(TextButton::buttonColourId, Colours::white);
	laf.setColour(TextButton::textColourOffId, Colour(0xff00b5f6));

	laf.setColour(TextButton::buttonOnColourId, laf.findColour(TextButton::textColourOffId));
	laf.setColour(TextButton::textColourOnId, laf.findColour(TextButton::buttonColourId));
}


void updateToggleState(Button* button, String name, int* currentOscillatorIndex, Slider* frequencyOffset, Slider* amplification, ElixerAudioProcessor& p)
{
	auto state = button->getToggleState();
	if (state) {
		*currentOscillatorIndex = atoi(button->getComponentID().getCharPointer());
		frequencyOffset->setValue(p.frequencyOffsets[*currentOscillatorIndex]);
		amplification->setValue(p.amplificationOffsets[*currentOscillatorIndex]);
	}
}

//==============================================================================
ElixerAudioProcessorEditor::ElixerAudioProcessorEditor(ElixerAudioProcessor& p)
	: AudioProcessorEditor(&p), processor(p)
{
	// Make sure that before the constructor has finished, you've set the
	// editor's size to whatever you need it to be.
	addAndMakeVisible(oscillatorCount);
	addAndMakeVisible(frequencyOffset);
	addAndMakeVisible(amplification);
	addAndMakeVisible(oscillatorCountLable);
	addAndMakeVisible(frequencyOffsetLable);
	addAndMakeVisible(amplificationLable);
	currentOscillatorIndex = 0;

	for (size_t i = 0; i < 10; i++)
	{
		addAndMakeVisible(oscillatorButtons[i]);
		if (i == 0)
			oscillatorButtons[i].setToggleState(true, NotificationType::sendNotification);
		oscillatorButtons[i].setComponentID((String)i);
		oscillatorButtons[i].setRadioGroupId(1, NotificationType::dontSendNotification);
		oscillatorButtons[i].onClick = [this, i, &p] { updateToggleState(&oscillatorButtons[i], (String)i, &currentOscillatorIndex, &frequencyOffset, &amplification, p); };
	}

	oscillatorCount.setRange(1, 10);
	oscillatorCount.setTextValueSuffix("");
	oscillatorCount.setValue(1);
	oscillatorCount.onValueChange = [this, &p] { oscillatorCount.setValue(round(oscillatorCount.getValue())); p.numberOfOscillators = (int)oscillatorCount.getValue(); p.prepareToPlay(p.getSampleRate(), 0); };
	
	frequencyOffset.setRange(-200, 200);
	frequencyOffset.setTextValueSuffix("Hz");
	frequencyOffset.setValue(0);
	frequencyOffset.onValueChange = [this, &p] { p.frequencyOffsets[currentOscillatorIndex] = frequencyOffset.getValue(); p.prepareToPlay(p.getSampleRate(), 0); };

	amplification.setRange(0, 1);
	amplification.setTextValueSuffix("");
	amplification.setValue(0);
	amplification.onValueChange = [this, &p] { p.amplificationOffsets[currentOscillatorIndex] = amplification.getValue(); p.prepareToPlay(p.getSampleRate(), 0); };
	
	setSize(800, 350);
	setLookAndFeel(new LookAndFeel_V4());
}

ElixerAudioProcessorEditor::~ElixerAudioProcessorEditor()
{
}

//==============================================================================
void ElixerAudioProcessorEditor::paint(Graphics& g)
{
	// (Our component is opaque, so we must completely fill the background with a solid colour)
	g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
	
	g.setColour(Colours::white);
	g.setFont(15.0f);
	g.drawRect(30, 40, getWidth() - 60, getHeight() - 100);

	for (size_t i = 0; i < 10; i++)
	{
		oscillatorButtons[i].setBounds(-1100, 10, 10, 10);
	}

	for (size_t i = 0; i < oscillatorCount.getValue(); i++)
	{
		oscillatorButtons[i].setBounds((getWidth() / 2 - 5 * (oscillatorButtons[i].getWidth() + 10)) + i * (oscillatorButtons[i].getWidth() + 10), 10, 30, 20);
	}
	
	oscillatorCount.setBounds(10, getHeight() - 10 - oscillatorCount.getHeight(), getWidth() - 10, 20);
	frequencyOffset.setBounds(60, 120, getWidth() - 120, 20);
	amplification.setBounds(60, getHeight() - amplification.getHeight() - 90, getWidth() - 120, 20);

	oscillatorCountLable.setBounds(10, getHeight() - 10 - oscillatorCount.getHeight() - oscillatorCountLable.getHeight(), getWidth() - 10, 20);
	frequencyOffsetLable.setBounds(60, 90, getWidth() - 10, 20);
	amplificationLable.setBounds(60, getHeight() - amplification.getHeight() - amplificationLable.getHeight() - 100, getWidth() - 10, 20);

	oscillatorCountLable.setText("Oscillator Count", NotificationType::dontSendNotification);
	frequencyOffsetLable.setText("Frequency Offset", NotificationType::dontSendNotification);
	amplificationLable.setText("Amplification", NotificationType::dontSendNotification);
}

void ElixerAudioProcessorEditor::resized()
{
	// This is generally where you'll want to lay out the positions of any
	// subcomponents in your editor..
}

