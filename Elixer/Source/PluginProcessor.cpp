#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
ElixerAudioProcessor::ElixerAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
	: AudioProcessor(BusesProperties()
#if ! JucePlugin_IsMidiEffect
#if ! JucePlugin_IsSynth
		.withInput("Input", AudioChannelSet::stereo(), true)
#endif
		.withOutput("Output", AudioChannelSet::stereo(), true)
#endif
	)
#endif
{
}

ElixerAudioProcessor::~ElixerAudioProcessor()
{
}

//==============================================================================
const String ElixerAudioProcessor::getName() const
{
	return JucePlugin_Name;
}

bool ElixerAudioProcessor::acceptsMidi() const
{
#if JucePlugin_WantsMidiInput
	return true;
#else
	return false;
#endif
}

bool ElixerAudioProcessor::producesMidi() const
{
#if JucePlugin_ProducesMidiOutput
	return true;
#else
	return false;
#endif
}

bool ElixerAudioProcessor::isMidiEffect() const
{
#if JucePlugin_IsMidiEffect
	return true;
#else
	return false;
#endif
}

double ElixerAudioProcessor::getTailLengthSeconds() const
{
	return 0.0;
}

int ElixerAudioProcessor::getNumPrograms()
{
	return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
				// so this should be at least 1, even if you're not really implementing programs.
}

int ElixerAudioProcessor::getCurrentProgram()
{
	return 0;
}

void ElixerAudioProcessor::setCurrentProgram(int index)
{
}

const String ElixerAudioProcessor::getProgramName(int index)
{
	return {};
}

void ElixerAudioProcessor::changeProgramName(int index, const String& newName)
{
}

//==============================================================================
void ElixerAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
	// Use this method as the place to do any pre-playback
	// initialisation that you need..
	oscillators.clear();

	for (auto i = 0; i < numberOfOscillators; ++i)
	{
		auto* oscillator = new SineOscillator();
		auto frequency = 440 * pow(2.0, (midiNote - 69.0) / 12.0) + frequencyOffsets[i];
		oscillator->setFrequency((float)frequency, (float)sampleRate);
		oscillator->setVolume(amplificationOffsets[i]);
		oscillators.add(oscillator);
	}

	level = 0.25f / numberOfOscillators;
}

void ElixerAudioProcessor::releaseResources()
{
	// When playback stops, you can use this as an opportunity to free up any
	// spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool ElixerAudioProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
#if JucePlugin_IsMidiEffect
	ignoreUnused(layouts);
	return true;
#else
	// This is the place where you check if the layout is supported.
	// In this template code we only support mono or stereo.
	if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
		&& layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
		return false;

	// This checks if the input layout matches the output layout
#if ! JucePlugin_IsSynth
	if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
		return false;
#endif

	return true;
#endif
}
#endif

void logMessage(const String messaage)
{
	Logger::outputDebugString(messaage);
}

static double getMidiCode(const MidiMessage& m)
{
	if (m.isNoteOn()) {
		return m.getNoteNumber();
	}

	if (m.isNoteOff()) return m.getNoteNumber() + 1000;
	return -1;
}

void ElixerAudioProcessor::processBlock(AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
	ScopedNoDenormals noDenormals;
	auto totalNumInputChannels = getTotalNumInputChannels();
	auto totalNumOutputChannels = getTotalNumOutputChannels();

	MidiBuffer::Iterator iterator(midiMessages);
	MidiMessage message;
	int sampleNumber;
	while (iterator.getNextEvent(message, sampleNumber))
	{

		message.setTimeStamp(sampleNumber / this->getSampleRate());
		auto midiCode = getMidiCode(message);
		logMessage((String)midiCode);
		if (midiCode >= 0);
		{
			if (midiCode < 1000)
				midiNote = midiCode;
			else if (midiCode - 1000 == midiNote)
				midiNote = 0;
			prepareToPlay(this->getSampleRate(), 0);
		}
	}
	midiMessages.clear(midiMessages.getFirstEventTime(), midiMessages.getNumEvents());

	if (midiNote != 0)
		for (auto oscillatorIndex = 0; oscillatorIndex < oscillators.size(); ++oscillatorIndex)
		{
			auto* oscillator = oscillators.getUnchecked(oscillatorIndex);

			for (auto sample = 0; sample < buffer.getNumSamples(); ++sample)
			{
				auto levelSample = oscillator->getNextSample() * oscillator->getVolume();
				buffer.addSample(0, sample, levelSample);
				buffer.addSample(1, sample, levelSample);
			}
		}
}

//==============================================================================
bool ElixerAudioProcessor::hasEditor() const
{
	return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* ElixerAudioProcessor::createEditor()
{
	return new ElixerAudioProcessorEditor(*this);
}

//==============================================================================
void ElixerAudioProcessor::getStateInformation(MemoryBlock& destData)
{
	// You should use this method to store your parameters in the memory block.
	// You could do that either as raw data, or use the XML or ValueTree classes
	// as intermediaries to make it easy to save and load complex data.
	destData.append(frequencyOffsets, 8*10);
	destData.append(amplificationOffsets, 8 * 10);
	destData.append(&numberOfOscillators, 4);
}

void ElixerAudioProcessor::setStateInformation(const void* data, int sizeInBytes)
{
	// You should use this method to restore your parameters from this memory block,
	// whose contents will have been created by the getStateInformation() call.
	/*frequencyOffsets = (double *)data*/

	for (size_t i = 0; i < 10; i++)
	{
		frequencyOffsets[i] = ((double*)data)[i];
	}
	for (size_t i = 0; i < 10; i++)
	{
		amplificationOffsets[i] = ((double*)data)[i+10];
	}
	numberOfOscillators = ((int*)data)[sizeInBytes - 3];

	prepareToPlay(this->getSampleRate(), 0);
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
	return new ElixerAudioProcessor();
}
